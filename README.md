# kassie's dotfiles
hi, im kassie, these are my dotfiles.
i maintain them using yadm, it's mainly just zsh with configs using presto.
ill add stuff here as i need it :)

## how to use
1. install yadm:
```shell
$ sudo apt install yadm
```
2. clone repo
```shell
$ yadm clone https://gitlab.com/kassie3point11/dotfiles.git
# or
$ yadm clone git@gitlab.com:kassie3point11/dotfiles.git
```
3. grab submodules
```shell
$ yadm submodule update --init --recursive
```
4. change shells
```shell
$ chsh -s /bin/sh
```
5. open a new shell