#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# convenience functions for path manipulation
function prependpath() {
  path=($1 $path)
}
function appendpath() {
  path=($path $1)
}

function prependfpath() {
  fpath=($1 $fpath)
}
function appendfpath() {
  fpath=($fpath $1)
}

# append ~/bin and ~/.local/bin to path
for local_bin_dir in $HOME/bin $HOME/.local/bin; do
  [[ -d $local_bin_dir ]] && prependpath $local_bin_dir
done

# i've got a bit of a weird setup here with `oss-cad-suite` (a premade set of tools for hardware dev - see https://github.com/YosysHQ/oss-cad-suite-build)
# pretty much, i couldn't get it working right on windows fully, and i wanted it under wsl anyway. however, for that to work, you have to have certain tools 
# called using the windows setup instead. so, what i do here is this:
# if oss-cad-suite isn't present at all, skip it
# if it is, lets:
# - define a function to activate the environment (oss-cad-suite-activate)
# - if $OSS_CAD_SUITE_ROOT/windows exists, also do this:
#   - add $OSS_CAD_SUITE_ROOT/windows/bin-light to the path
#   - add a function (oss-cad-suite-win-build) to:
#     - copy $OSS_CAD_SUITE_ROOT/windows/lib/*.dll to bin-light
#     - copy tools that need direct hardware access (just iceprog i think?) from windows/bin to bin-light
OSS_CAD_SUITE_ROOT=$HOME/oss-cad-suite
if [[ -d $OSS_CAD_SUITE_ROOT ]]; then
  if [[ -d ${OSS_CAD_SUITE_ROOT}/linux ]]; then
    OSS_CAD_SUITE_LIN=$OSS_CAD_SUITE_ROOT/linux
    if [[ -d $OSS_CAD_SUITE_ROOT/windows ]]; then
      OSS_CAD_SUITE_WIN=$OSS_CAD_SUITE_ROOT/windows
    fi
  else
    OSS_CAD_SUITE_LIN=$OSS_CAD_SUITE_ROOT
  fi
  if (( ${+OSS_CAD_SUITE_WIN} )); then
    function oss-cad-suite-win-build() {
      rm $OSS_CAD_SUITE_WIN/bin-light -rf
      mkdir -p $OSS_CAD_SUITE_WIN/bin-light
      cp $OSS_CAD_SUITE_WIN/lib/* $OSS_CAD_SUITE_WIN/bin-light -rvs
      
      for tool in iceprog; do
        ln -sv $OSS_CAD_SUITE_WIN/bin/$tool.exe $OSS_CAD_SUITE_WIN/bin-light/$tool
        chmod -v +x $OSS_CAD_SUITE_WIN/bin-light/$tool
      done
      hash -r 2>/dev/null
    }
  fi
  function oss-cad-suite-activate() {
    source $OSS_CAD_SUITE_LIN/environment
    if (( ${+OSS_CAD_SUITE_WIN} )); then
      if [[ ! -d $OSS_CAD_SUITE_WIN/bin-light ]]; then
        echo "bin-light dir not found, build it with oss-cad-suite-win-build"
      fi
      prependpath $OSS_CAD_SUITE_WIN
      hash -r 2>/dev/null
    fi
  }
fi

# stuff for me learning kubernetes, tried to set it up so it only does stuff if it finds tools installed
(( $+commands[kubectl] )) && source <(kubectl completion zsh)
KREW_ROOT=${KREW_ROOT:-$HOME/.krew}
[[ -f $KREW_ROOT/bin/kubectl-krew ]] && prependpath $KREW_ROOT/bin || unset KREW_ROOT
KUBECTX_PATH=${KUBECTX_PATH:-/opt/kubectx}
if [[ -d $KUBECTX_PATH ]]; then
  prependfpath $KUBECTX_PATH/completion
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
